package com.edutec.materialdesign.profile;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;

import com.edutec.materialdesign.R;
import com.edutec.materialdesign.bottom.fragment.BottomFragment;
import com.edutec.materialdesign.floating.fragment.FloatingFragment;
import com.edutec.materialdesign.home.fragment.HomeFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class ProfileActivity extends AppCompatActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        connect();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.nav_item_profile);

        BottomNavigationView navigationView = findViewById(R.id.navigation);
        navigationView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);
        displayView(new HomeFragment());
    }

    private BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            Fragment fragment;
            switch (menuItem.getItemId()) {
                case R.id.navigation_email:
                    toolbar.setTitle("Shop");
                    fragment = new HomeFragment();
                    displayView(fragment);
                    return true;
                case R.id.navigation_plus:
                    toolbar.setTitle("Plus");
                    fragment = new FloatingFragment();
                    displayView(fragment);
                    return true;
                case R.id.navigation_shop:
                    toolbar.setTitle("Shop");
                    fragment = new BottomFragment();
                    displayView(fragment);
                    return true;
                case R.id.navigation_alarm:
                    toolbar.setTitle("Alarm");
                    fragment = new FloatingFragment();
                    displayView(fragment);
                    return true;
                case R.id.navigation_inbox:
                    toolbar.setTitle("Inbox");
                    fragment = new FloatingFragment();
                    displayView(fragment);
                    return true;
            }
            return false;
        }
    };

    private void displayView(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.commit();
    }

    private void connect() {
        toolbar = findViewById(R.id.toolbar);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: // flechita para regresar
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
